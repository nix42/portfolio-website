# portfolio-website

Source files for my portfolio website hosted on github pages

https://nix42.github.io

## Features

- HTML, CSS, JavaScript
- DOM
- Responsive

## Notes

Please feel free to use this code under the GNU GPL 3.0 license 